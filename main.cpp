#include <QAudioDeviceInfo>
#include <QQuickView>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtDebug>

int main(int argc, char *argv[])
{
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));
    
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    for (auto dev : QAudioDeviceInfo::availableDevices(QAudio::AudioOutput)) {
        qDebug() << "@ Audio device = " << dev.deviceName();
    }
    qDebug() << "@ Default audio device = " << QAudioDeviceInfo::defaultOutputDevice().deviceName();

    
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty()) {
        return -1;
    }
    

    /*
    QQuickView view(QString("qrc:/main.qml"));

    if (view.status() == QQuickView::Error)
        return -1;
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.show();
    */

    return app.exec();
}