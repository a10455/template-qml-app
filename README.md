# Template QML Application
This repository answers a simple question:
"how to use linux to execute a single graphical application on an embedded device that might not have a keyboard attached?"

This repository contains a simple QML application modified to demonstrate the Qt virtualkeyboard,
and everything necessary to create a snap containing this application,
so that it can either be used standalone, on a desktop or to create an embedded
product that only run this specific application full-screen (as well as other non-graphical snaps).

## Building
In order to build the snap an Ubuntu 20.04 LTS Virtual Machine is needed.

When the VM is ready install required stuff:
```sh
sudo apt update
sudo apt install git
sudo snap install snapcraft --classic
```

and then compile the snap (the application will be compiled as part of the snap creation)

```sh
snapcraft
```

This will leave you with at least a my-snap-name_0.1_arch.snap file in the project root directory.

## Installing the snap

Section to fill

## Ubuntu Core VM for testing

First download the Ubuntu Core 20.04 img.xz file for amd64, then execute the following command to extract the file:

```sh
xzcat xzcat ubuntu-core-20-amd64.img.xz > uc.img
qemu-img resize uc.img +15GB
```

After that the original file can be removed.

For convenience you can start Ubuntu Core to test out the system via the provided bash script:

```sh
./start-uc.sh
```

Remember that you have to install qemu and ovmf for that to work.

After the VM is started configure the system following on-screen instructions (remember to use an email address registered to Ubuntu One).

You can then login to this system using:

```sh
ssh <Ubuntu SSO username>@localhost -p 8022
```

In addition to SSH access, QEMU will start a VNC-shared console which can be accessed at vnc://localhost:5900 on the system.

## Useful link
In no particular order:
'''
https://forum.snapcraft.io/t/make-desktop-qt-5-application-run-with-mir-kiosk-on-ubuntu-core/14989
https://ubuntu.com/tutorials/wayland-kiosk#4-set-up-test--development-environment
https://github.com/pachulo/musescore-snap/tree/master/snap
https://github.com/MirServer/mir-kiosk-kodi
https://github.com/MirServer/iot-example-graphical-snap/tree/master/snap
https://snapcraft.io/docs/snapcraft-overview
https://discourse.ubuntu.com/t/on-screen-keyboard-support-in-ubuntu-frame/25840
https://discourse.ubuntu.com/t/the-graphics-core20-snap-interface/23000
https://discourse.ubuntu.com/t/snaps-to-develop-a-web-kiosk-on-ubuntu-core-using-wayland/6424
https://ubuntu.com/core/docs/testing-with-qemu
'''